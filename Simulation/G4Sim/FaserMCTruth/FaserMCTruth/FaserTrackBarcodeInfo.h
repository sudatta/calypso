/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FaserTrackBarcodeInfo_H
#define FaserTrackBarcodeInfo_H

#include "FaserVTrackInformation.h"

namespace ISF {
  class FaserISFParticle;
}

class FaserTrackBarcodeInfo: public FaserVTrackInformation {
public:
        FaserTrackBarcodeInfo(int bc, const ISF::FaserISFParticle* baseIsp=0);
	int GetParticleBarcode() const;
	const ISF::FaserISFParticle *GetBaseISFParticle() const;
	void SetBaseISFParticle(const ISF::FaserISFParticle*);
	void SetReturnedToISF(bool returned);
	bool GetReturnedToISF() const;

private:
	const ISF::FaserISFParticle *m_theBaseISFParticle;
	int m_barcode;
	bool m_returnedToISF;
};


#endif // FaserTrackBarcodeInfo_H
